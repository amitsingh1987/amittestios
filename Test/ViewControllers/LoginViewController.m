//
//  ViewController.m
//  Test
//
//  Created by CafeX-Dev on 2/13/17.
//  Copyright © 2017 Usoft. All rights reserved.
//

#import "LoginViewController.h"
#import "UITextField+Additions.h"
#import "MBProgressHUD.h"
#import "Constants.h"
#import "UserAccount.h"
#import "TSRequest.h"
#import "TSRequest+WebServiceMethods.h"
#import "NSDictionary+APIResponse.h"
#import "RegisterViewController.h"
#import "WorkflowTableViewController.h"

@interface LoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.title = @"Login";
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

#pragma mark - Private Methods

- (BOOL)doFieldsValidation
{
    if ([self.userNameTextField isTextFieldEmpty])
    {
    
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Please enter user name"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    
        [alert show];

        
        [self.userNameTextField becomeFirstResponder];
        
        return NO;
    }
    else if ([self.passwordTextField isTextFieldEmpty])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Please enter your password"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        
        [self.passwordTextField becomeFirstResponder];
        
        return NO;
    }
    
    return YES;
}

- (void)performLogin
{
    UserAccount *account = [[UserAccount alloc]init];
    account.username = self.userNameTextField.text;
    account.password = self.passwordTextField.text;
    self.userNameTextField.text = @"";
    self.passwordTextField.text = @"";
    
    
    TSRequest *request = [TSRequest signInRequestWithAccount:account];
    [request setRequestCompletionBlock:^(NSDictionary *response, NSError *error) {
        
        
        if (response)
        {
            if ([response responseMessage].length>0)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[response responseMessage]
                                                                       delegate:nil
                                                                       cancelButtonTitle:@"Ok"
                                                                       otherButtonTitles:nil];
                 
                 [alert show];
            }
            else{
                
                appDelegate.bearerToken = [response objectForKey:@"access_token"];//refresh_token//access_token
                
                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
                
                UINavigationController *nav = (UINavigationController *)[sb instantiateInitialViewController];
                
                [self.navigationController presentViewController:nav animated:NO completion:nil];
                  nav = nil;
            }
        }
        else
        {
            
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"Please enter valid user name and password."
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
            
            [alert show];
            
            
            [self.userNameTextField becomeFirstResponder];
        }
    }];

}


#pragma mark - UIControl Methods

- (IBAction)loginButtonPressed:(UIButton *)sender
{
    if ([self doFieldsValidation])
    {
        [self.view endEditing:YES];
        [self performLogin];
    }
}

- (IBAction)registerButtonPressed:(UIButton *)sender
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    RegisterViewController *viewController = (RegisterViewController *)[sb instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
    viewController = nil;
}

#pragma mark - UITextFieldDelegate Methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ((range.location == 0) && ([string isEqualToString:@" "]))
    {
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == _userNameTextField)
    {
        [_passwordTextField becomeFirstResponder];
    }
    else if(textField == _passwordTextField)
    {
        [_passwordTextField resignFirstResponder];
        [self loginButtonPressed:_loginButton];
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField trimWhiteSpacesAndNewline];
}





@end
