//
//  RegisterViewController.m
//  Test
//
//  Created by CafeX-Dev on 2/13/17.
//  Copyright © 2017 Usoft. All rights reserved.
//

#import "RegisterViewController.h"
#import "MBProgressHUD.h"
#import "Constants.h"
#import "UserAccount.h"
#import "TSRequest.h"
#import "TSRequest+WebServiceMethods.h"
#import "NSDictionary+APIResponse.h"
#import "UITextField+Additions.h"
#import "NSString+Additions.h"


@interface RegisterViewController ()

@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Register";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIControl Methods
- (IBAction)registerButtonPressed:(id)sender
{
    if ([self doFieldsValidation])
    {
    UserAccount *account = [[UserAccount alloc]init];
    account.username = self.userNameTextField.text;
    account.password = self.passwordTextField.text;
    account.email = self.emailTextField.text;
        
        self.userNameTextField.text = @"";
        self.passwordTextField.text = @"";
        self.emailTextField.text = @"";
    
    
    TSRequest *request = [TSRequest signUpRequestWithAccount:account];
    [request setRequestCompletionBlock:^(NSDictionary *response, NSError *error) {
        
        
        if (response)
        {
            if ([response responseMessage].length>0)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:                                                [response responseMessage]
                                                               delegate:nil
                                                     cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
                
                [alert show];
            }
        }
        else 
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:error.description
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
            
            [alert show];
            
            
            [self.userNameTextField becomeFirstResponder];
        }
    }];
    }
}


#pragma mark - Private Methods

- (BOOL)doFieldsValidation
{
    if ([self.userNameTextField isTextFieldEmpty] )
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Please enter user name"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        
        [alert show];

        [self.userNameTextField becomeFirstResponder];
        return NO;
    }
    else if (self.userNameTextField.text.length < 7)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Username should start with a string and be atleast 6 characters long"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        
        [alert show];
        
        [self.userNameTextField becomeFirstResponder];
        return NO;
    }
    else if ([self.emailTextField isTextFieldEmpty])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Please enter your email"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];

    
        [self.emailTextField becomeFirstResponder];
        return NO;
    }
    else if (![NSString isEmailValid:self.emailTextField.text])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Please enter your valid email"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];

        
        [self.emailTextField becomeFirstResponder];
        return NO;
    }
    else if ([self.passwordTextField isTextFieldEmpty])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Please enter your password"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];

        [self.passwordTextField becomeFirstResponder];
        return NO;
    }
    else if ([self.passwordTextField textByTrimmingWhiteSpacesAndNewline].length < 6)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Please enter minimum 6 character password"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];

        [self.passwordTextField becomeFirstResponder];
        return NO;
    }
    
    return YES;
}

#pragma mark - UITextFieldDelegate Methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ((textField.tag == 0) && ([string isEqualToString:@" "]))
    {
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _userNameTextField)
    {
        [_emailTextField becomeFirstResponder];
    }
    else if(textField == _emailTextField)
    {
        [_passwordTextField becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
        
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField trimWhiteSpacesAndNewline];
}


@end
