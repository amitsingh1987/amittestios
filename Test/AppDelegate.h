//
//  AppDelegate.h
//  Test
//
//  Created by CafeX-Dev on 2/13/17.
//  Copyright © 2017 Usoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *bearerToken;


@end

