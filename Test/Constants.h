//
//  Constants.h
//
//  Created by CafeX-Dev on 2/13/17.
//  Copyright (c) 2017 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Convenience method to replace NSLog output in DEBUG mode
 */
#ifdef DEBUG
    #define NSLog(args, ...) NSLog((@"%s [LineNumber: %d] " args), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
    #define NSLog(args, ...) NSLog((@"%s [LineNumber: %d] " args), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
//    #define NSLog(...)
#endif


/**
 * Return an object of appdelegate
 */
#define appDelegate         ((AppDelegate *)[[UIApplication sharedApplication] delegate])

/**
 * Website Url
 */
#define kRoot_Url      @"http://85.222.238.74/"
