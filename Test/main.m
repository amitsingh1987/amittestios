//
//  main.m
//  Test
//
//  Created by CafeX-Dev on 2/13/17.
//  Copyright © 2017 Usoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
