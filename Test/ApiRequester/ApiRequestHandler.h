//
//  ApiRequestHandler.h
//
//  Created by Arvind Singh on 01/10/13.
//  Copyright (c) 2013 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"



#define kHTTP_METHOD_POST   @"POST"
#define kHTTP_METHOD_GET    @"GET"
#define kHTTP_METHOD_PUT    @"PUT"
#define kHTTP_METHOD_DELETE @"DELETE"


@class ApiRequestHandler;

// The completion callback type. A nil error indicates success.
// Your callback should check to see if error is non-nil, and react accordingly.
// url and userInfo are used for context in the callback.
//typedef void (^ApiRequestHandlerCompletionHandler)(ApiRequestHandler *handler, NSError *error, NSURL *url, NSDictionary *userInfo, NSData *data);
typedef void (^ApiRequestHandlerCompletionHandler)(ApiRequestHandler *handler, NSError *error, NSURL *url, NSDictionary *userInfo, NSDictionary *serverDict);



@interface ApiRequestHandler : NSObject

@property (nonatomic, copy, readonly) ApiRequestHandlerCompletionHandler completionHandler;
@property (nonatomic, retain) NSURL *url;
@property (nonatomic, retain) NSDictionary *userInfo;
@property (nonatomic, retain) NSData *requestData;
@property (nonatomic, retain) NSString *responseTextEncoding;
@property (nonatomic, retain) MBProgressHUD *progressHud;
@property (nonatomic, retain) UIActivityIndicatorView *indicator;



-(id)initWithUrl:(NSURL*)url withRequestData:(NSData*)requestData withHttpMethod:(NSString*)httpMethod withPostLength:(NSString*)postLength addProgressHUDView : (UIView*)rootView hudTitle : (NSString*)hudTitle andCompletionHandler:(ApiRequestHandlerCompletionHandler)handler;

//-(id)initWithUrl:(NSURL*)url withRequestData:(NSData*)requestData withHttpMethod:(NSString*)httpMethod withPostLength:(NSString*)postLength addProgressHUDView : (UIView*)rootView hudControl:(id)uiControl hudTitle : (NSString*)hudTitle andCompletionHandler:(ApiRequestHandlerCompletionHandler)handler;

//+ (NSMutableURLRequest *)urlRequestForZipFileMultipartFormDataPostBody:(NSURL *)URL httpMethod:(NSString *)httpMethod additionalHeaders:(NSDictionary *)headersDict postData:(NSMutableArray *)postData fileData:(NSData *)fileData;

+ (NSMutableURLRequest *)urlRequestForMultipartFormDataPostBody:(NSURL *)URL httpMethod:(NSString *)httpMethod additionalHeaders:(NSDictionary *)headersDict postData:(NSMutableArray *)postData fileData:(NSMutableArray *)fileData;

- (id)initWithURLRequest:(NSMutableURLRequest *)urlRequest userInfo:(NSDictionary *)userInfo addProgressHUDView : (UIView*)navigationView hudTitle : (NSString*)hudTitle andCompletionHandler:(ApiRequestHandlerCompletionHandler)handler ;

- (id)initWithURLSession:(NSMutableURLRequest *)urlRequest userInfo:(NSDictionary *)userInfo addProgressHUDView : (UIView*)navigationView hudTitle:(NSString *)hudTitle andCompletionHandler:(ApiRequestHandlerCompletionHandler)handler;

// Cancels the connection.
- (void)cancel;

@end
