//
//  ApiRequestHandler.m
//
//  Created by Arvind Singh on 01/10/13.
//  Copyright (c) 2013 Appster. All rights reserved.
//

#import "ApiRequestHandler.h"
#import "Reachability.h"

@interface ApiRequestHandler ()<NSURLSessionDelegate>

@property (nonatomic, retain) NSURLConnection *internalConnection;
@property (nonatomic, retain) NSMutableData *internalData;
@property (nonatomic, retain) NSString *methodName;

//- (NSString *)HTTPMethod;

@end

@implementation ApiRequestHandler

@synthesize completionHandler = _completionHandler;
@synthesize url	= _url;
@synthesize userInfo = _userInfo;
@synthesize requestData = _requestData;
@synthesize responseTextEncoding = _responseTextEncoding;

@synthesize internalConnection = _internalConnection;
@synthesize internalData = _internalData;
@synthesize methodName = _methodName;


- (void)dealloc
{
   
}


- (id)initWithURLSession:(NSMutableURLRequest *)urlRequest userInfo:(NSDictionary *)userInfo addProgressHUDView:(UIView*)navigationView hudTitle:(NSString *)hudTitle andCompletionHandler:(ApiRequestHandlerCompletionHandler)handler
{
    
    if ((self = [super init]))
    {
        
        if([[self checkNetworkStatus] isEqualToString:@"There is internet connection"])
        {
            [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
            
            self.url = [urlRequest URL];
            self.userInfo = userInfo;
            _completionHandler = [handler copy];
            self.methodName = [urlRequest HTTPMethod];
            self.requestData = [urlRequest HTTPBody];
            self.internalData = [NSMutableData data];
            [urlRequest setTimeoutInterval:15.0];

            
            NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate:self delegateQueue: [NSOperationQueue mainQueue]];
            
            //    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
            //    [urlRequest setHTTPMethod:@"POST"];
            //    [urlRequest addValue:checksum forHTTPHeaderField:@"Md5Hash"];
            //    [urlRequest addValue:@"Keep-Alive" forHTTPHeaderField:@"Connection"];
            //    [urlRequest addValue:@"xml" forHTTPHeaderField:@"Content-Type"];
            //    [urlRequest setHTTPBody:file];
            
            [urlRequest addValue:@"Keep-Alive" forHTTPHeaderField:@"Connection"];

            NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithRequest:urlRequest];
            [dataTask resume];
            
        }
        else
        {
           
            
            NSError *error = [NSError errorWithDomain:@"com" code:10 userInfo:nil];
            if (handler)
            {
                handler(self, error, [urlRequest URL], userInfo, nil);
            }
        }
    }
    
    return self;
    
    
    
}


-(id)initWithUrl:(NSURL*)url withRequestData:(NSData*)requestData withHttpMethod:(NSString*)httpMethod withPostLength:(NSString*)postLength addProgressHUDView:(UIView *)navigationView  hudTitle:(NSString *)hudTitle andCompletionHandler:(ApiRequestHandlerCompletionHandler)handler
{
    if([[self checkNetworkStatus] isEqualToString:@"There is internet connection"])
    {
        if(navigationView !=nil)
        {
            _progressHud = [MBProgressHUD showHUDAddedTo:navigationView animated:YES];
            _progressHud.labelText = hudTitle;
        }
        else
        {
            [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
        }
        
        self.url = url;
        _completionHandler = [handler copy];
        self.methodName = httpMethod;
        self.requestData = requestData;
        
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        if(appDelegate.bearerToken.length > 0){
            
            NSLog(@"%@",appDelegate.bearerToken);
            
            NSString *authHeader = [NSString stringWithFormat:@"Bearer %@",appDelegate.bearerToken];

           
            [request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        }
        else{
            
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

        }
        
        [request setHTTPMethod:httpMethod];
        [request setHTTPBody:requestData];
       // [request setTimeoutInterval:15.0];
        
        self.internalData = [NSMutableData data];
        self.internalConnection = [NSURLConnection connectionWithRequest:request delegate:self];
    }
    else
    {
    
        NSError *error = [NSError errorWithDomain:@"com" code:10 userInfo:nil];
        if (handler)
        {
            handler(self, error, url, nil, nil);
        }

    }
    
    return self;
}

#pragma mark - Multipart/Form-Data Body Methods

+ (NSMutableURLRequest *)urlRequestForMultipartFormDataPostBody:(NSURL *)URL httpMethod:(NSString *)httpMethod additionalHeaders:(NSDictionary *)headersDict postData:(NSMutableArray *)postData fileData:(NSMutableArray *)fileData
{
    //NSLog(@"%@", @"\r\n==== Building a multipart/form-data body ====");
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:URL];
    // Add http method
    [urlRequest setHTTPMethod:httpMethod];

    
    NSStringEncoding stringEncoding = NSUTF8StringEncoding;
	NSString *charset = (NSString *)CFStringConvertEncodingToIANACharSetName(CFStringConvertNSStringEncodingToEncoding(stringEncoding));
	
	// We don't bother to check if post data contains the boundary, since it's pretty unlikely that it does.
	CFUUIDRef uuid = CFUUIDCreate(nil);
	NSString *uuidString = (NSString*)CFBridgingRelease(CFUUIDCreateString(nil, uuid));
	CFRelease(uuid);
	NSString *stringBoundary = [NSString stringWithFormat:@"0xKhTmLbOuNdArY-%@",uuidString];
    //NSLog(@"stringBoundary: %@", stringBoundary);
    
    // Add default httpheader fields ***********
    [urlRequest addValue:[NSString stringWithFormat:@"multipart/form-data; charset=%@; boundary=%@", charset, stringBoundary] forHTTPHeaderField:@"Content-Type"];
    // Add additional httpheader fields
    if (headersDict != nil)
    {
        for (NSString *key in [headersDict allKeys])
        {
            [urlRequest addValue:[headersDict objectForKey:key] forHTTPHeaderField:key];
        }
    }
    
    
    // Build multipart form data post body ***********
	NSMutableData *postBody = [NSMutableData data];
	[postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:stringEncoding]];
	
	// Adds post data ***********
	NSString *endItemBoundary = [NSString stringWithFormat:@"\r\n--%@\r\n", stringBoundary];
	NSUInteger i = 0;
	for (NSDictionary *val in postData)
    {
		[postBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",[val objectForKey:@"key"]] dataUsingEncoding:stringEncoding]];
		[postBody appendData:[[val objectForKey:@"value"] dataUsingEncoding:stringEncoding]];
		i++;
        
        // Only add the boundary if this is not the last item in the post body
		if (i != [postData count] || [fileData count] > 0)
        {
			[postBody appendData:[endItemBoundary dataUsingEncoding:stringEncoding]];
		}
	}
	
	// Adds files to upload ***********
	i = 0;
	for (NSDictionary *val in fileData)
    {
        [postBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", [val objectForKey:@"key"], [val objectForKey:@"fileName"]] dataUsingEncoding:stringEncoding]];
		[postBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", [val objectForKey:@"contentType"]] dataUsingEncoding:stringEncoding]];
		
		id data = [val objectForKey:@"data"];
		if ([data isKindOfClass:[NSString class]])
        {
            NSData *d = [NSData dataWithContentsOfFile:data];
			[postBody appendData:d];
		}
        else
        {
			[postBody appendData:data];
		}
		i++;
        
		// Only add the boundary if this is not the last item in the post body
		if (i != [fileData count])
        {
            [postBody appendData:[endItemBoundary dataUsingEncoding:stringEncoding]];
		}
	}
	
	[postBody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",stringBoundary] dataUsingEncoding:stringEncoding]];
    [urlRequest setHTTPBody:postBody];
	
    //NSLog(@"%@", @"\r\n==== End of multipart/form-data body ====");
    
    return urlRequest;
}


#pragma mark - NSURLConnectionDelegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.responseTextEncoding = [response textEncodingName];
	[self.internalData setLength:0];
    
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    if(_progressHud)
    {
        [_progressHud hide:YES];
    }
    else
    {
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];

    }
    
   // ShowAlert(@"", error.description);
    
  	if (self.completionHandler)
    {
		self.completionHandler(self, error, self.url, self.userInfo, nil);
	}
    
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[self.internalData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if(_progressHud)
    {
        [_progressHud hide:YES];
    }
    else
    {
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];

    }
    
    
    NSData *data = [NSData dataWithData:self.internalData];

    NSString *responseString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
    
    NSLog(@"%@",responseString);
    
    NSError* error;

    NSMutableDictionary *dict = [NSJSONSerialization
                          JSONObjectWithData:data //1
                          
                          options:kNilOptions
                          error:&error];//[data objectFromJSONData];
    
    if(dict.count<=0 && responseString.length>0)
    {
        dict = [[NSMutableDictionary alloc]init];
        [dict setValue:responseString forKey:@"message"];
    }
    

    
	if (self.completionHandler)
    {
        self.completionHandler(self, nil, self.url, self.userInfo, dict);
	}
    
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
	// Cleanup. Handy if you modify this class to support multiple connections from the same instance.
	self.internalData = nil;
	self.internalConnection = nil;
}

- (void)cancel
{
    [self.internalConnection cancel];
    
	self.internalConnection = nil;
	self.internalData = nil;
}

- (NSString*)checkNetworkStatus
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
       return @"There is no internet connection";
    }
    else
    {
        return @"There is internet connection";
    }
}


#pragma mark URLSession Delegate - 

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data
{
    [self.internalData appendData:data];
   // self.internalData = [NSMutableData dataWithData:data];

   // NSString * str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
   // NSLog(@"Received String %@",str);
}



- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    if (error)
    {
        NSLog(@"%@ failed: %@", task.originalRequest.URL, error);
    }
    
    NSString * str = [[NSString alloc] initWithData:self.internalData encoding:NSUTF8StringEncoding];
    NSLog(@"Received String = %@",str);
    
    NSDictionary *dict = [NSJSONSerialization
                          JSONObjectWithData:self.internalData //1
                          
                          options:kNilOptions
                          error:&error];//[data objectFromJSONData];
    
    if (!dict)
    {
        NSMutableDictionary *errorDict = [NSMutableDictionary dictionaryWithObjects:[NSArray arrayWithObjects:NSLocalizedString(@"An error occurred while performing the operation. Please try again later.", @"An error occurred while performing the operation. Please try again later."), nil] forKeys:[NSArray arrayWithObjects:@"message", nil]];
        dict = [NSMutableDictionary dictionaryWithObject:errorDict forKey:@"result"];
    }
    

	if (self.completionHandler)
    {
        self.completionHandler(self, error, self.url, self.userInfo, dict);
	}
    
	//[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
	// Cleanup. Handy if you modify this class to support multiple connections from the same instance.
	self.internalData = nil;
	self.internalConnection = nil;
}


/*- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session
{
    
    NSLog(@"All tasks are finished");
}*/

@end
