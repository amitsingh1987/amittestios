//
//  UITextField+Additions.m
//  Test
//
//  Created by CafeX-Dev on 2/13/17.
//  Copyright © 2017 Usoft. All rights reserved.
//

#import "UITextField+Additions.h"

@implementation UITextField (Additions)

- (NSString *)textByTrimmingWhiteSpacesAndNewline
{
    NSCharacterSet *whitespaceAndNewline = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmedString = [self.text stringByTrimmingCharactersInSet:whitespaceAndNewline];
    self.text = trimmedString;
    
    return trimmedString;
}

- (BOOL)isTextFieldEmpty
{
    NSString *str = self.text; 
    return ([str length] == 0);
}

- (void)trimWhiteSpacesAndNewline
{
    NSCharacterSet *whitespaceAndNewline = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmedString = [self.text stringByTrimmingCharactersInSet:whitespaceAndNewline];
    self.text = trimmedString;
}

@end
