//
//  NSString+Additions.m
//
//  Created by Amit Singh on 13/02/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import "NSString+Additions.h"

@implementation NSString (Additions)

+ (NSString *)urlEncode:(NSString *)inputString
{
    /*return (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(NULL,
     (__bridge CFStringRef)inputString,
     NULL,
     (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
     CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));*/
    
    CFStringRef encodedCfStringRef = CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef)inputString,NULL,(CFStringRef)@"!*'\"();@+$,%#[]% ",kCFStringEncodingUTF8 );
    NSString *endcodedString = (NSString *)CFBridgingRelease(encodedCfStringRef);
    return endcodedString;
}

+ (BOOL)isEmailValid:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+ (id)formattedValue:(id)value
{
    if(value)
    {
        if([value isKindOfClass:[NSNull class]])
        {
            return [NSString string];
        }
        else if(([value caseInsensitiveCompare:@"null"] == NSOrderedSame) || ([value caseInsensitiveCompare:@"(null)"] == NSOrderedSame))
        {
            return [NSString string];
        }
        else
        {
            return value;
        }
    }
    return [NSString string];
}

+ (NSString *)removeCommaBetweenStrings:(NSString *)firstString secondString:(NSString *)secondString
{
    NSString *resultSring = @"";
    
    if (secondString.length > 0)
    {
        resultSring = [firstString stringByAppendingFormat:@", %@", [NSString formattedValue:secondString]];
    }
    else
    {
        resultSring = [NSString formattedValue:firstString];
    }
    
    return resultSring;
}

+ (NSString *)replaceSingleQuoteString:(NSString *)singleQouteString
{
    return [singleQouteString stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
}

+ (BOOL)validateUrl:(NSString *)urlString
{
    NSString *urlRegEx = nil;
    
    if (([urlString rangeOfString:@"http"].location == 0) || ([urlString rangeOfString:@"https"].location == 0))
    {
        urlRegEx = @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    }
    else
    {
        urlRegEx = @"((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    }

    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:urlString];
}

// Below code is used to convert the imogi in utf8 string to send on server
+ (NSString *)convertToUtf8String:(NSString *)inputString
{
    NSData *data = [inputString dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    return string;
}

// Below code is used to convert the utf8 string to string with imogi
+ (NSString *)convertToASCIIString:(NSString *)inputString
{
    NSData *imogiStringData = [inputString dataUsingEncoding:NSUTF8StringEncoding
                                          allowLossyConversion:YES];
    NSString *string = [[NSString alloc] initWithData:imogiStringData encoding:NSNonLossyASCIIStringEncoding];
    
    return string;
}

+ (NSString *)stringFromTimeInterval:(NSTimeInterval)timeInterval
{
    NSString *sText = @"";
    
    if (timeInterval < 60)
    {
        sText = NSLocalizedString(@"just now", @"");
    }
    else if (timeInterval < 3600)
    {
        timeInterval = round(timeInterval / 60);
        if (timeInterval == 0) timeInterval = 1;
        
        sText = [NSString stringWithFormat:@"%.0f %@ %@", timeInterval, (timeInterval > 1) ? NSLocalizedString(@"mins", @""):NSLocalizedString(@"min", @""), NSLocalizedString(@"ago", @"")];
    }
    else if (timeInterval < 86400)
    {
        timeInterval = round(timeInterval / 3600);
        if (timeInterval == 0) timeInterval = 1;
        
        sText = [NSString stringWithFormat:@"%.0f %@ %@", timeInterval, (timeInterval > 1) ? NSLocalizedString(@"hours", @""):NSLocalizedString(@"hour", @""), NSLocalizedString(@"ago", @"")];
    }
    else
    {
        timeInterval = round(timeInterval / 86400);
        if (timeInterval == 0) timeInterval = 1;
        
        sText = [NSString stringWithFormat:@"%@ %.0f %@ %@", NSLocalizedString(@"ago-prefix", @""), timeInterval, (timeInterval > 1) ? NSLocalizedString(@"days", @""):NSLocalizedString(@"day", @""), NSLocalizedString(@"ago-suffix", @"")];
    }
    
    return sText;
}

@end
