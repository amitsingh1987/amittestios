//
//  NSString+Additions.h
//
//  Created by Amit Singh on 13/02/15.
//  Copyright (c) 2015 Appster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)

+ (NSString *)urlEncode:(NSString *)inputString;

+ (BOOL)isEmailValid:(NSString *)email;
+ (id)formattedValue:(id)value;

+ (NSString *)removeCommaBetweenStrings:(NSString *)firstString secondString:(NSString *)secondString;

+ (NSString *)replaceSingleQuoteString:(NSString *)singleQouteString;

+ (BOOL)validateUrl:(NSString *)urlString;

+ (NSString *)convertToUtf8String:(NSString *)inputString;

+ (NSString *)convertToASCIIString:(NSString *)inputString;

@end
