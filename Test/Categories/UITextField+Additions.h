//
//  UITextField+Additions.h
//  Test
//
//  Created by CafeX-Dev on 2/13/17.
//  Copyright © 2017 Usoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Additions)

- (NSString *)textByTrimmingWhiteSpacesAndNewline;

- (BOOL)isTextFieldEmpty;

- (void)trimWhiteSpacesAndNewline;

@end

