//
//  TSRequest+WebServiceMethods.m
//  TrolleySaver
//
//  Created by Amit Kumar Singh on 25/09/14.
//  Copyright (c) 2014 appster. All rights reserved.
//

#import "TSRequest+WebServiceMethods.h"
#import "UserAccount.h"
#import "Constants.h"
#import "ApiRequestHandler.h"
#import "NSDictionary+APIResponse.h"


@implementation TSRequest (WebServiceMethods)


+ (TSRequest *)signUpRequestWithAccount:(UserAccount *)account
{
    TSRequest *request = [[TSRequest alloc] initWithName:[NSString stringWithFormat:@"%@register", kRoot_Url]];
    [request.parameters setObject:account.username forKey:@"username"];
    [request.parameters setObject:@"amit" forKey:@"name"];
    [request.parameters setObject:account.email forKey:@"email"];
    [request.parameters setObject:account.password forKey:@"plainPassword"];
   
    request.method = PutMethod;
    
    [request sendRequest:request];
    return request;
}

+ (TSRequest *)signInRequestWithAccount:(UserAccount *)account
{
    TSRequest *request = [[TSRequest alloc] initWithName:[NSString stringWithFormat:@"%@oauth/v2/token", kRoot_Url]];
    [request.parameters setObject:account.username forKey:@"username"];
    [request.parameters setObject:account.password forKey:@"password"];
    [request.parameters setObject:@"password" forKey:@"grant_type"];
    [request.parameters setObject:@"5836f395acf3f0f975807d11_3rnd3q1v5y4gg8s4c88w0848g0gow00c8ggsgws0w00kgoc808" forKey:@"client_id"];
    [request.parameters setObject:@"62erhw408yo0c4oc0480wwccc0cww44s4k4sg0c40s88gw4400" forKey:@"client_secret"];
    request.method = PostMethod;
    
    [request sendRequest:request];
    return request;
}

+ (TSRequest *)getAllWorkflows
{
    TSRequest *request = [[TSRequest alloc] initWithName:[NSString stringWithFormat:@"%@api/v1/workflows", kRoot_Url]];
    request.method = GetMethod;
    
    [request sendRequest:request];
    return request;
}


#pragma mark - send Request

- (void)sendRequest:(TSRequest*)request
{
  NSString *jsonString =   [self.parameters jsonStringWithPrettyPrint:YES];
    
   NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", self.name]];
    
    ApiRequestHandler *handler=[[ApiRequestHandler alloc] initWithUrl:url withRequestData:requestData withHttpMethod:request.method withPostLength:nil addProgressHUDView:nil hudTitle:nil andCompletionHandler:^(ApiRequestHandler *handler, NSError *error, NSURL *url, NSDictionary *userInfo, id responseDict) {
        
        NSLog(@"sendRequest responseDict:%@",responseDict);
        
        if (responseDict)
        {
            
            if(self.requestCompletionBlock)
                self.requestCompletionBlock(responseDict,error);
            
        }
        else
        {
            if(self.requestCompletionBlock)
                self.requestCompletionBlock(responseDict,error);
        }
        
    }];
    
    NSLog(@"ApiRequestHandler: %@", handler);
}


@end
