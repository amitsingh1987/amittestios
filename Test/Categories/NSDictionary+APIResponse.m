//
//  NSDictionary+APIResponse.m
//  TrolleySaver
//
//  Created by Amit Kumar Singh on 26/09/14.
//  Copyright (c) 2014 appster. All rights reserved.
//

#import "NSDictionary+APIResponse.h"

@implementation NSDictionary (APIResponse)

- (BOOL)isResponseSuccess
{
    NSDictionary *resultDict = [self  objectForKey:@"result"];
    if (resultDict)
    {
        if ([[resultDict objectForKey:@"status"] isEqualToString:@"success"])
        {
            return YES;
        }
    }
    
    return NO;
}

- (NSString *)responseMessage
{
    NSString *message = @"";
   
    message = [self  objectForKey:@"message"];
    
    return message;
}

- (BOOL)isEmpty
{
    return (([self count] == 0) ? YES : NO);
}

-(NSString*) jsonStringWithPrettyPrint:(BOOL) prettyPrint {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:(NSJSONWritingOptions)    (prettyPrint ? NSJSONWritingPrettyPrinted : 0)
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

@end
