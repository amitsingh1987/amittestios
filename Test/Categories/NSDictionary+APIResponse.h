//
//  NSDictionary+APIResponse.h
//  TrolleySaver
//
//  Created by Amit Kumar Singh on 26/09/14.
//  Copyright (c) 2014 appster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (APIResponse)

- (BOOL)isResponseSuccess;
- (NSString *)responseMessage;
- (BOOL)isEmpty;
-(NSString*) jsonStringWithPrettyPrint:(BOOL) prettyPrint;

@end
