//
//  TSRequest+WebServiceMethods.h
//  TrolleySaver
//
//  Created by Amit Kumar Singh on 25/09/14.
//  Copyright (c) 2014 appster. All rights reserved.
//

#import "TSRequest.h"
@class UserAccount;

@interface TSRequest (WebServiceMethods)

+ (TSRequest *)signUpRequestWithAccount:(UserAccount *)account;
+ (TSRequest *)signInRequestWithAccount:(UserAccount *)account;
+ (TSRequest *)getAllWorkflows;

@end
