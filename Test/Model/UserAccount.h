//
//  TSAccount.h
//  TrolleySaver
//
//  Created by Amit Kumar Singh on 25/09/14.
//  Copyright (c) 2014 appster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserAccount : NSObject

@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *email;

@end
