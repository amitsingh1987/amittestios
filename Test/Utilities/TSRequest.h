//
//  TSRequest.h
//  TrolleySaver
//
//  Created by Amit Kumar Singh on 25/09/14.
//  Copyright (c) 2014 appster. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void(^WebServiceCompletionBlock)(NSDictionary *response, NSError *error);

extern NSString * const PostMethod;
extern NSString * const GetMethod;
extern NSString * const PutMethod;

extern NSUInteger const FromIndexDefault;
extern NSUInteger const BatchSizeDefault;

@interface TSRequest : NSObject

@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *method;
@property (nonatomic) BOOL multipart;
@property (nonatomic) BOOL pagingEnabled;
@property (nonatomic) NSUInteger batchSize;
@property (nonatomic) NSUInteger fromIndex;
@property (nonatomic,strong) NSMutableDictionary *parameters;
@property (nonatomic,strong) WebServiceCompletionBlock requestCompletionBlock;

- (id)initWithName:(NSString *)name;

- (id)initWithName:(NSString *)name parameters:(NSDictionary *)parameters;

- (id)initWithName:(NSString *)name
        parameters:(NSDictionary *)parameters
   callbackHandler:(WebServiceCompletionBlock)callbackHandler;

+ (void)nilComplitionHandler;


@end
