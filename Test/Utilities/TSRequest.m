//
//  TSRequest.m
//  TrolleySaver
//
//  Created by Amit Kumar Singh on 25/09/14.
//  Copyright (c) 2014 appster. All rights reserved.
//

#import "TSRequest.h"

NSString * const PostMethod = @"POST";
NSString * const GetMethod = @"GET";
NSString * const PutMethod = @"PUT";
NSUInteger const FromIndexDefault = 0;
NSUInteger const BatchSizeDefault = 40;

@implementation TSRequest

+ (void)nilComplitionHandler
{
    
}

- (id)initWithName:(NSString *)name
{
	self = [super init];
	if (self)
	{
        self.fromIndex = FromIndexDefault;
        self.batchSize = BatchSizeDefault;
		self.name = name;
		self.parameters = [NSMutableDictionary new];
	}
	return self;
}

- (id)initWithName:(NSString *)name parameters:(NSDictionary *)parameters
{
	self = [self initWithName:name];
	if (self)
	{
		self.name = name;
		self.parameters = [parameters mutableCopy];
	}
	return self;
}

- (id)initWithName:(NSString *)name
        parameters:(NSDictionary *)parameters
   callbackHandler:(WebServiceCompletionBlock)callbackHandler
{
	self = [self initWithName:name parameters:parameters];
	if (self)
	{
		self.requestCompletionBlock = callbackHandler;
	}
	return self;
}



@end
